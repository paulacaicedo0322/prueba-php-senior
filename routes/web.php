<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\WeatherController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\HistoricalHumidityController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [HomeController::class, 'index'])->name('home');
Route::get('/consult-humidity', [HomeController::class, 'humidity_consult'])->name('humidity.consult');
Route::get('/consult-historical', [HomeController::class, 'historical'])->name('historical.consult');

Route::post('/search', [WeatherController::class, 'search'])->name('search');
Route::post('/search-historical', [HistoricalHumidityController::class, 'search'])->name('search.historical');
