@extends('layouts.app')

@section('css_extra')	
<style>
    #lienzoMapa {
	width: 100%;
    height: 550px;
    padding: 5px;
    border-radius: 10px; 
}
    .home{
    margin: 3%;
    /* margin-left: 20%;
    margin-right: 20%;
    padding: 10%; */
   }
</style>
@endsection
@section('content')
<div class="home">
<div class="container">
    <div class="row">
        <div class="col-sm-4">
            <label for="">Seleccione su ciudad</label>
            <select name="city" class="form-control select_city" id="select_city">
                <option selected readonly hidden>-- Seleccione una Ciudad --</option>
                @foreach ($cities as $city)
                    <option value="{{$city->id}}">{{$city->name}}</option>
                @endforeach
            </select>
            <br>
        </div>
        <div class="col-lg-8">
            <div id="lienzoMapa"></div>
        </div>
    </div>
</div>
</div>

@endsection
@section('js_extra')
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAqgCT6gyO_Pssax2Ow8ZyDOc-7PRJCIM4&callback"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<script>
    toastr.options.preventDuplicates = true;

$.ajaxSetup({
    headers:{
        'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
    }
});
</script>
<script>
const cities = @json($cities);

function initialize() {
    map = new google.maps.Map(document.getElementById("lienzoMapa"), {
        center: new google.maps.LatLng(39.39929341484949, -100.04502444266068),
        zoom: 4,
    });

const iconBase = "{{asset('icons/png')}}";
const icons = {
	city: {
		icon: iconBase + "/city2.png",
	},            
};

const infowindow = new google.maps.InfoWindow;

        document.addEventListener('change', (event) => {
            if(event.target.classList.contains('select_city')){
                var city = event.target.value;
                console.log(city);
                var url = '<?= route("search") ?>';
                    $.post(url, {city:city}, function(data){
                        if (data.code == 1) {
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: data.title,
                            html: data.msg,
                            showConfirmButton: true
                        });
                        var city = data.city;
                        var idMap	= city.id;
                        var name 	= city.name;
                        var coords_lat= city.latitude;
                        var coords_lng	= city.longitude;
                        var humidity = data.humidity;
                        var point 	= new google.maps.LatLng(coords_lat, coords_lng);
                        console.log(name);
                        const contentString = 
                        '<div id="content"' +
                            '<div id="siteNotice">' +
                            "</div>" +
                            '<h1 id="firstHeading" class="firstHeading">'+name+'</h1>' +
                            '<div id="bodyContent">' +
                                '<br>'+
                                '<br>'+
                                '<div class="row justify-content-between">'+
                                    '<div class="col-sm-12">'+
                                        '<h3>Humedad: '+humidity+'% </h3>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        "</div>";

                            var marker = new google.maps.Marker({
                                map:map,
                                position: point,
                                icon: icons.city.icon
                            });
                            
                            marker.addListener('click', function(){
                                infowindow.setContent(contentString);
                                infowindow.open(map, marker); 
                            });
                            document.addEventListener('change', (event) => {
                                if(event.target.classList.contains('select_city')){
                                    marker.setMap(null);
                        }});

                        }else{
                        Swal.fire({
                            position: 'center',
                            icon: 'error',
                            title: data.msg,
                            showConfirmButton: false,
                            timer: 3000
                        });
                        }
                    },'json');
            //Fin fucion principal
            };
        });
// // fin google maps 
  }
</script>
@endsection
