@extends('layouts.app')

@section('css_extra')	
<style>
    a:link, a:hover, a:visited, a:active, a {
    text-decoration:none;
    color:black;
}
   .home{
    margin: 10%;
    /* margin-left: 20%;
    margin-right: 20%;
    padding: 10%; */
   }
   h3{
    text-align: justify;
   }
  
</style>
@endsection
@section('content')
<div class="home">
    <div class="row">
        <div class="col">
            <a class="" href="{{route('humidity.consult')}}">
            <div class="card mb-3" style="max-width: 540px;">
                <div class="row g-0">
                  <div class="col-md-4">
                    <img src="{{asset('images/city.png')}}" class="img-fluid rounded-start" alt="...">
                  </div>
                  <div class="col-md-8">
                    <div class="card-body">
                      <h5 class="card-title">Humedad en su ciudad</h5>
                      <p class="card-text">Mire la ubicación de su ciudad en el mapa y obtenga el porcentaje de humedad en su ciudad.</p>
                      {{-- <p class="card-text"><small class="text-muted">Última actualización hace 3 minutos</small></p> --}}
                    </div>
                  </div>
                </div>
              </div>
            </a>
        </div>
        <div class="col">
            <a class="" href="{{route('historical.consult')}}">
            <div class="card mb-3" style="max-width: 540px;">
                <div class="row g-0">
                  <div class="col-md-4">
                    <img src="{{asset('images/historial.png')}}" class="img-fluid rounded-start" alt="...">
                  </div>
                  <div class="col-md-8">
                    <div class="card-body">
                      <h5 class="card-title">Historial de humedad</h5>
                      <p class="card-text">Revise el historial del porcentaje de humedad en su ciudad.</p>
                      {{-- <p class="card-text"><small class="text-muted">Última actualización hace 3 minutos</small></p> --}}
                    </div>
                  </div>
                </div>
              </div>
            </a>
        </div>
    </div>
</div>

@endsection
