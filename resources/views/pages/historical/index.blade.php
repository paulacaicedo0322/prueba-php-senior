@extends('layouts.app')

@section('css_extra')	
<style>
    #lienzoMapa {
	width: 100%;
    height: 700px;
    padding: 5px;
    border-radius: 10px; 
}
    .home{
    margin: 3%;
    /* margin-left: 20%;
    margin-right: 20%;
    padding: 10%; */
   }
</style>
@endsection
@section('content')
<div class="home">
<div class="container">
    <div class="row">
        <div class="col-sm-4">
            <label for="">Seleccione su ciudad</label>
            <select name="city" class="form-control select_city" id="select_city">
                <option selected readonly hidden>-- Seleccione una Ciudad --</option>
                @foreach ($cities as $city)
                    <option value="{{$city->id}}">{{$city->name}}</option>
                @endforeach
            </select>
            <br>
        </div>
        <div class="col-lg-8">
            <label for="">Registros de humedad en la ciudad</label>
            <table class="table table-responsive">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Ciudad</th>
                    <th scope="col">Latitud</th>
                    <th scope="col">Longitud</th>
                    <th scope="col">Humedad</th>
                    <th scope="col">Fecha</th>
                  </tr>
                </thead>
                <tbody id="tds">
                
                </tbody>
              </table>
        </div>
    </div>
</div>
</div>

@endsection
@section('js_extra')
<script>
    toastr.options.preventDuplicates = true;

$.ajaxSetup({
    headers:{
        'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
    }
});
</script>
<script>
        document.addEventListener('change', (event) => {
            if(event.target.classList.contains('select_city')){
                const elements = document.getElementsByClassName('delte_tr');
                while(elements.length > 0){
                    elements[0].parentNode.removeChild(elements[0]);
                }
                var city = event.target.value;
                console.log(city);
                var url = '<?= route("search.historical") ?>';
                    $.post(url, {city:city}, function(data){
                        if (data.code == 1) {
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: data.title,
                            html: data.msg,
                            showConfirmButton: true
                        });
                        var historical = data.historical;

                        console.log(historical);
                        Array.prototype.forEach.call(historical, function(historicalElements) {
                            // var date =  new Date(historicalElements.created_at);
                            // var formattedDate = date.format('Y-m-d H:i:s');
                            var date = new Date(historicalElements.created_at).toLocaleDateString('es-cl', { weekday:"long", year:"numeric", month:"short", day:"numeric", hour:"numeric", minute:"numeric"})
                            $('#tds').append(`<tr class="delte_tr">
                            <th scope="row">${historicalElements.id}</th>
                            <td>${historicalElements.city.name}</td>
                            <td>${historicalElements.city.latitude}</td>
                            <td>${historicalElements.city.longitude}</td>
                            <td>${historicalElements.humidity}%</td>
                            <td>${date}</td>
                            </tr>`);
                        });
                            
                        }else{
                        Swal.fire({
                            position: 'center',
                            icon: 'error',
                            title: data.msg,
                            showConfirmButton: false,
                            timer: 3000
                        });
                        }
                    },'json');
            //Fin fucion principal
            };
        });
// // fin google maps 
</script>
@endsection
