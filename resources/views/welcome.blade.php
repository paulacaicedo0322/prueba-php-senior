@extends('layouts.app')

@section('css_extra')	
<style>
    #lienzoMapa {
	width: 90%;
    height: 900px;
    margin: 0;
    border: 0px; 
}
</style>
@endsection
@section('content')
<h2>Hola</h2>
<div id="lienzoMapa">

</div>
@endsection
@section('js_extra')
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAqgCT6gyO_Pssax2Ow8ZyDOc-7PRJCIM4&callback"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<script>
function initialize() {
    map = new google.maps.Map(document.getElementById("lienzoMapa"), {
        center: new google.maps.LatLng(40.0000000, -4.0000000),
        zoom: 12,
    });
  }
</script>
@endsection
