<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'latitude',
        'longitude',
        'status'
    ];

    public function detalle_venta(){
        return $this->hasMany(HistoricalHumidity::class,'city_id');
    }
}
