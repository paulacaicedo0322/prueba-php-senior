<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HistoricalHumidity extends Model
{
    use HasFactory;
    protected $table = 'historical_humidity';
    protected $fillable = [
        'city_id',
        'humidity'
    ];

    public function city(){
        return $this->belongsTo(City::class, 'city_id');
    }
}
