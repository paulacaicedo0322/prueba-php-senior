<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Models\City;
use App\Models\HistoricalHumidity;

class WeatherController extends Controller
{
    public function search(Request $request){
        $city = City::find($request->city);
    
        $city_name = $city->name;
        $key = config('services.owm.key');
    
        $response = Http::get("https://api.openweathermap.org/data/2.5/weather?q=".$city_name."&lang=es"."&appid=".$key)->json();
        
        if($response['cod'] == "200") {
          $humidity = $response['main']['humidity'];
          $country = $response['sys']['country'];
          $ok = $response['cod'];

          $historical = new HistoricalHumidity();
          $historical->city_id = $city->id;
          $historical->humidity = $humidity;
          $historical->save();

          return response()->json(['code' => 1,'title'=>'¡Consulta realizada!', 'msg'=>'De clic en el icono que aparecerá en el mapa, y revise el porcentaje de humedad en: '.$city_name,'humidity'=>$humidity, 'city'=>$city, 'country'=>$country, 'ok'=>$ok]);
          
        } else {
          return response()->json(['code' => 0, 'msg'=>'Error, Intentelo mas tarde']);
        }
    }
}
