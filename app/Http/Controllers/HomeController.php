<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\City;

class HomeController extends Controller
{
    public function index(){
        $menu = 0;
        return view('pages.home', compact('menu'));
    }

    public function humidity_consult(){
        $cities = City::all();

    return view('pages.map.index', compact('cities'));
    }

    public function historical(){
        $cities = City::all();

    return view('pages.historical.index', compact('cities'));
    }
}
