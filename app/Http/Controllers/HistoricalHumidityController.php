<?php

namespace App\Http\Controllers;
use App\Models\HistoricalHumidity;
use Illuminate\Http\Request;

class HistoricalHumidityController extends Controller
{
    function search(Request $request){
        $city_id = $request->city;

        $historical = HistoricalHumidity::with('city')->where('city_id', $city_id)->orderby('created_at', 'desc')->get();

        return response()->json(['code' => 1,'title'=>'¡Consulta realizada!', 'msg'=>'Aqui encontrara el registro de las consultas realizadas', 'historical'=>$historical]);

    }
}
