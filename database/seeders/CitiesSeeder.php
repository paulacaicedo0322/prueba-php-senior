<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class CitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cities')->insert([
            'name' => 'Miami',
            'latitude' => '25.76322214996493',
            'longitude' => '-80.19100920546538',
            'status' => '0',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('cities')->insert([
            'name' => 'New York',
            'latitude' => '40.7158881444145',
            'longitude' => '-74.02503922239052',
            'status' => '0',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('cities')->insert([
            'name' => 'Orlando',
            'latitude' => '28.537216096761142',
            'longitude' => '-81.3797554400396',
            'status' => '0',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
